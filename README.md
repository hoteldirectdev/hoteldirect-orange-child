=== Hoteldirect Orange Theme ===
Contributors: Hotel Direct ID
Tags: simple, company profile
Requires at least: 4.8
Stable tag: v1.2.1
Version: 1.2.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Description ==
Hoteldirect Company Profile


== Installation ==
This section describes how to install the child theme and get it working.

1. You need to install and activate the parent theme first, indohotelswp
2. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.


== Changelog ==

= 1.2.1 =
* adjust style

= 1.2.0 =
* tidy up

= 1.1.3 =
* Adjust color text booking engine

= 1.1.2 =
* Adjust booking engine
* remove list-style on page contact

= 1.1.1 =
* Adjust booking engine

= 1.1.0 =
* Tidy up booking form for mobile
