## 14 January 2019 - v1.2.1 ##
* Adjust style

## 07 January 2019 - v1.2.0 ##
* Tidy up

## 18 July 2018 - v1.1.3 ##
* Adjust color text booking engine

## 18 July 2018 - v1.1.2 ##
* Adjust booking engine
* remove list-style on page contact

## 18 July 2018 - v1.1.1 ##
* Adjust booking engine

## 18 July 2018 - v1.1.0 ##
* Tidy up booking form for mobile
